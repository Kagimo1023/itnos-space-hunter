/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#ifndef SH2_INPUT_H
#define SH2_INPUT_H

#include <stdint.h>

typedef struct pointer const *pointer;

_Bool pointer_try_lock (pointer);
_Bool pointer_try_unlock (pointer);
_Bool pointer_get_if_locked (pointer);

#ifdef SH2_INPUT_PRIVATE

union handle
{
  void *p;
  uintmax_t u;
};

struct input_driver
{
  _Bool (*pointer_try_lock)(union handle);
  _Bool (*pointer_try_unlock)(union handle);
  _Bool (*pointer_get_if_locked)(union handle);
};

struct pointer
{
  struct input_driver const *driver;
  union handle handle;
};

#endif

#endif
