/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#define SH2_INPUT_PRIVATE
#include "input.h"

_Bool
pointer_try_lock (register struct pointer const *const dev)
{
  return dev->driver->pointer_try_lock (dev->handle);
}

_Bool
pointer_try_unlock (register struct pointer const *const dev)
{
  return dev->driver->pointer_try_unlock (dev->handle);
}

_Bool
pointer_get_if_locked (register struct pointer const *const dev)
{
  return dev->driver->pointer_get_if_locked (dev->handle);
}
