/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#define SH2_EVENT_PRIVATE
#include <stdlib.h>
#include <assert.h>
#include "event.h"
#include "eventsource.h"
#include "timerevent.h"

struct timer_event
{
  struct event base;
  enum timer_event_type type;
};

struct timer_periodic_event
{
  struct timer_event base;
  uintmax_t elapsed;
};

enum timer_event_type
timer_event_get_type (register struct event const *const e)
{
  assert (event_source_get_type (event_get_source (e)) == EVENT_SOURCE_TIMER);
  return ((struct timer_event const *)e)->type;
}

struct event const *
timer_event_periodic_create (register event_source const source,
                             register uintmax_t const elapsed)
{
  assert (event_source_get_type (source) == EVENT_SOURCE_TIMER);
  register struct timer_periodic_event *const e = malloc (sizeof *e);
  e->base.base.source = source;
  e->base.type = TIMER_EVENT_PERIODIC;
  e->elapsed = elapsed;
  return &e->base.base;
}

uintmax_t
timer_event_periodic_get_elapsed_time (register struct event const *const e)
{
  assert (event_source_get_type (event_get_source (e)) == EVENT_SOURCE_TIMER);
  assert (timer_event_get_type (e) == TIMER_EVENT_PERIODIC);
  return ((struct timer_periodic_event const *)e)->elapsed;
}
