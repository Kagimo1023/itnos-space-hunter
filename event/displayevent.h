/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#ifndef SH2_DISPLAYEVENT_H
#define SH2_DISPLAYEVENT_H

#include "event.h"
#include "eventsource.h"
#include "../video/video.h"

enum display_event_type
{
  DISPLAY_EVENT_CONNECTED,
  DISPLAY_EVENT_DISPLAY_SURFACE_RESIZED,
  DISPLAY_EVENT_DISPLAY_SURFACE_DESTROYED
};

enum display_event_type display_event_get_type (event);

event display_event_connected_create (event_source);
event display_event_display_surface_resized_create (event_source,
                                                    display_surface_id,
                                                    uintmax_t,
                                                    uintmax_t);
display_surface_id display_event_display_surface_resized_get_display_surface_id (event);
uintmax_t display_event_display_surface_resized_get_width (event);
uintmax_t display_event_display_surface_resized_get_height (event);

#endif
