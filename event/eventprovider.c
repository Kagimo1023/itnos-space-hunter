/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#define SH2_EVENT_PROVIDER_PRIVATE
#include "eventprovider.h"

void
event_provider_init (register struct event_provider const *const provider)
{
  provider->init ();
}

void
event_provider_set_callback (register struct event_provider const *const provider,
                             register event_provider_callback const callback,
                             register void *const user_argument)
{
  provider->set_callback (callback, user_argument);
}

void
event_provider_update (register struct event_provider const *const provider)
{
  provider->update ();
}
