/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#ifndef SH2_TIMEREVENT_H
#define SH2_TIMEREVENT_H

#include "event.h"
#include "eventsource.h"

enum timer_event_type
{
  TIMER_EVENT_PERIODIC
};

enum timer_event_type timer_event_get_type (event);

event timer_event_periodic_create (event_source, uintmax_t);
uintmax_t timer_event_periodic_get_elapsed_time (event);

#endif
