/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#define SH2_EVENT_PRIVATE
#include <stdlib.h>
#include <assert.h>
#include "event.h"
#include "eventsource.h"
#include "displayevent.h"

struct display_event
{
  struct event base;
  enum display_event_type type;
};

struct display_surface_resized_event
{
  struct display_event base;
  display_surface_id surface;
  uintmax_t width, height;
};

enum display_event_type
display_event_get_type (register struct event const *const e)
{
  assert (event_source_get_type (event_get_source (e)) == EVENT_SOURCE_DISPLAY);
  return ((struct display_event const *)e)->type;
}

struct event const *
display_event_connected_create (register event_source const source)
{
  assert (event_source_get_type (source) == EVENT_SOURCE_DISPLAY);
  register struct display_event *const e = malloc (sizeof *e);
  e->base.source = source;
  e->type = DISPLAY_EVENT_CONNECTED;
  return &e->base;
}

struct event const *
display_event_display_surface_resized_create (register event_source const source,
                                              register display_surface_id const surface,
                                              register uintmax_t const width,
                                              register uintmax_t const height)
{
  assert (event_source_get_type (source) == EVENT_SOURCE_DISPLAY);
  register struct display_surface_resized_event *const e = malloc (sizeof *e);
  e->base.base.source = source;
  e->base.type = DISPLAY_EVENT_DISPLAY_SURFACE_RESIZED;
  e->surface = surface;
  e->width = width;
  e->height = height;
  return &e->base.base;
}

display_surface_id
display_event_display_surface_resized_get_display_surface_id (register struct event const *const e)
{
  assert (event_source_get_type (event_get_source (e)) == EVENT_SOURCE_DISPLAY);
  assert (display_event_get_type (e) == DISPLAY_EVENT_DISPLAY_SURFACE_RESIZED);
  return ((struct display_surface_resized_event const *)e)->surface;
}

uintmax_t
display_event_display_surface_resized_get_width (register struct event const *const e)
{
  assert (event_source_get_type (event_get_source (e)) == EVENT_SOURCE_DISPLAY);
  assert (display_event_get_type (e) == DISPLAY_EVENT_DISPLAY_SURFACE_RESIZED);
  return ((struct display_surface_resized_event const *)e)->width;
}

uintmax_t
display_event_display_surface_resized_get_height (register struct event const *const e)
{
  assert (event_source_get_type (event_get_source (e)) == EVENT_SOURCE_DISPLAY);
  assert (display_event_get_type (e) == DISPLAY_EVENT_DISPLAY_SURFACE_RESIZED);
  return ((struct display_surface_resized_event const *)e)->height;
}
