/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#define SH2_EVENT_PRIVATE
#include <stdlib.h>
#include <assert.h>
#include "event.h"
#include "eventsource.h"
#include "pointerevent.h"

struct pointer_event
{
  struct event base;
  enum pointer_event_type type;
};

struct pointer_movement_event
{
  struct pointer_event base;
  intmax_t dx, dy;
};

struct pointer_wheel_movement_event
{
  struct pointer_event base;
  intmax_t d;
};

enum pointer_event_type
pointer_event_get_type (register struct event const *const e)
{
  assert (event_source_get_type (event_get_source (e)) == EVENT_SOURCE_POINTER);
  return ((struct pointer_event const *)e)->type;
}

struct event const *
pointer_event_movement_create (register event_source const source,
                               register intmax_t const dx,
                               register intmax_t const dy)
{
  assert (event_source_get_type (source) == EVENT_SOURCE_POINTER);
  register struct pointer_movement_event *const e = malloc (sizeof *e);
  e->base.base.source = source;
  e->base.type = POINTER_EVENT_MOVEMENT;
  e->dx = dx;
  e->dy = dy;
  return &e->base.base;
}

intmax_t
pointer_event_movement_get_delta_x (register struct event const *const e)
{
  assert (event_source_get_type (event_get_source (e)) == EVENT_SOURCE_POINTER);
  assert (pointer_event_get_type (e) == POINTER_EVENT_MOVEMENT);
  return ((struct pointer_movement_event const *)e)->dx;
}

intmax_t
pointer_event_movement_get_delta_y (register struct event const *const e)
{
  assert (event_source_get_type (event_get_source (e)) == EVENT_SOURCE_POINTER);
  assert (pointer_event_get_type (e) == POINTER_EVENT_MOVEMENT);
  return ((struct pointer_movement_event const *)e)->dy;
}

struct event const *
pointer_event_wheel_movement_create (register event_source const source,
                                     register intmax_t const d)
{
  assert (event_source_get_type (source) == EVENT_SOURCE_POINTER);
  register struct pointer_wheel_movement_event *const e = malloc (sizeof *e);
  e->base.base.source = source;
  e->base.type = POINTER_EVENT_WHEEL_MOVEMENT;
  e->d = d;
  return &e->base.base;
}

intmax_t
pointer_event_wheel_movement_get_delta (register struct event const *const e)
{
  assert (event_source_get_type (event_get_source (e)) == EVENT_SOURCE_POINTER);
  assert (pointer_event_get_type (e) == POINTER_EVENT_WHEEL_MOVEMENT);
  return ((struct pointer_wheel_movement_event const *)e)->d;
}

struct event const *
pointer_event_click_create (register event_source const source)
{
  assert (event_source_get_type (source) == EVENT_SOURCE_POINTER);
  register struct pointer_event *const e = malloc (sizeof *e);
  e->base.source = source;
  e->type = POINTER_EVENT_CLICK;
  return &e->base;
}

struct event const *
pointer_event_release_create (register event_source const source)
{
  assert (event_source_get_type (source) == EVENT_SOURCE_POINTER);
  register struct pointer_event *const e = malloc (sizeof *e);
  e->base.source = source;
  e->type = POINTER_EVENT_RELEASE;
  return &e->base;
}
