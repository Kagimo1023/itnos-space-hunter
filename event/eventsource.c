/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#include <stdlib.h>
#include "../video/video.h"
#include "../input/input.h"
#include "eventsource.h"

struct event_source
{
  char const *name;
  enum event_source_type type;
};

struct display_event_source
{
  struct event_source base;
  display d;
};

struct pointer_event_source
{
  struct event_source base;
  pointer p;
};

struct event_source const *
display_event_source_create (register char const *const name,
                             register display const d)
{
  register struct display_event_source *const source = malloc (sizeof *source);
  source->base.name = name;
  source->base.type = EVENT_SOURCE_DISPLAY;
  source->d = d;
  return (struct event_source const *)source;
}

display
display_event_source_get_display (register struct event_source const *const source)
{
  return ((struct display_event_source const *)source)->d;
}

struct event_source const *
timer_event_source_create (register char const *const name)
{
  register struct event_source *const source = malloc (sizeof *source);
  source->name = name;
  source->type = EVENT_SOURCE_TIMER;
  return source;
}

struct event_source const *
pointer_event_source_create (register char const *const name,
                             register pointer const p)
{
  register struct pointer_event_source *const source = malloc (sizeof *source);
  source->base.name = name;
  source->base.type = EVENT_SOURCE_POINTER;
  source->p = p;
  return &source->base;
}

pointer
pointer_event_source_get_pointer (register struct event_source const *const source)
{
  return ((struct pointer_event_source const *)source)->p;
}

enum event_source_type
event_source_get_type (register struct event_source const *const source)
{
  return source->type;
}

char const *
event_source_get_name (register struct event_source const *const source)
{
  return source->name;
}
