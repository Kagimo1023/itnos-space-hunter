/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#ifndef SH2_POINTEREVENT_H
#define SH2_POINTEREVENT_H

#include "event.h"
#include "eventsource.h"

enum pointer_event_type
{
  POINTER_EVENT_MOVEMENT,
  POINTER_EVENT_WHEEL_MOVEMENT,
  POINTER_EVENT_CLICK,
  POINTER_EVENT_RELEASE
};

enum pointer_event_type pointer_event_get_type (event);

event pointer_event_movement_create (event_source, intmax_t, intmax_t);
intmax_t pointer_event_movement_get_delta_x (event);
intmax_t pointer_event_movement_get_delta_y (event);
event pointer_event_wheel_movement_create (event_source, intmax_t);
intmax_t pointer_event_wheel_movement_get_delta (event);
event pointer_event_click_create (event_source);
event pointer_event_release_create (event_source);

#endif
