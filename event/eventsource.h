/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#ifndef SH2_EVENTSOURCE_H
#define SH2_EVENTSOURCE_H

#include "../video/video.h"
#include "../input/input.h"
#include "event.h"

enum event_source_type
{
  EVENT_SOURCE_TIMER,
  EVENT_SOURCE_POINTER,
  EVENT_SOURCE_KEYBOARD,
  EVENT_SOURCE_DISPLAY
};
typedef struct event_source const *event_source;

enum event_source_type event_source_get_type (event_source);
char const *event_source_get_name (event_source);
event_source display_event_source_create (char const *, display);
display display_event_source_get_display (event_source);
event_source timer_event_source_create (char const *);
event_source pointer_event_source_create (char const *, pointer);
pointer pointer_event_source_get_pointer (event_source);

#endif
