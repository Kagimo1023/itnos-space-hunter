/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#ifndef SH2_EVENTPROVIDER_H
#define SH2_EVENTPROVIDER_H

#include "event.h"

typedef struct event_provider const *event_provider;
typedef void (*event_provider_callback)(event, void *);

void event_provider_init (event_provider);
void event_provider_set_callback (event_provider,
                                  event_provider_callback,
                                  void *);
void event_provider_update (event_provider);

#ifdef SH2_EVENT_PROVIDER_PRIVATE

struct event_provider
{
  void (*init)(void);
  void (*set_callback)(event_provider_callback, void *);
  void (*update)(void);
};

#endif

#endif
