/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>
   Copyright (C) 2021 Daniil Alexeev <daniilxcxc2@gmail.com>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#ifndef SH2_COMMON_H
#define SH2_COMMON_H

#include <stdint.h>

#define length(a) (sizeof (a) / sizeof *(a))

struct point
{
  uintmax_t x, y;
};

struct rectangle
{
  struct point p1, p2;
};

inline uintmax_t
absolute_difference (register uintmax_t const x,
                     register uintmax_t const y)
{
  return x > y ? x - y : y - x;
}

inline uintmax_t
rectangle_width (register struct rectangle const r)
{
  return absolute_difference (r.p1.x, r.p2.x);
}

inline uintmax_t
rectangle_height (register struct rectangle const r)
{
  return absolute_difference (r.p1.y, r.p2.y);
}

#endif
