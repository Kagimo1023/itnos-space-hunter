/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "refcount.h"

struct ref
{
  void *p;
  uintmax_t refs;
};

struct ref *
ref_count_wrap (register void *const p)
{
  register struct ref *const r = malloc (sizeof *r);
  r->p = p;
  r->refs = 1;
  return r;
}

void *
ref_count_unwrap (register struct ref *const r)
{
  return r->p;
}

struct ref *
ref_count_share (register struct ref *const r)
{
  return r->refs++, r;
}

void
ref_count_unshare (register struct ref *const r)
{
  assert (r->refs);
  if (!--r->refs)
    {
      fprintf (stderr, "Freeing inaccessible datum at (%p)\n", r->p);
      free (r->p);
      free (r);
    }
}
