/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#ifndef SH2_TERRAIN_H
#define SH2_TERRAIN_H

#include "../common.h"
#include "../refcount.h"

#define CHUNK_WIDTH 100
#define CHUNK_HEIGHT 100

typedef struct terrain const *terrain;
typedef struct terrain_backstore const *terrain_backstore;
typedef unsigned char tile;
struct chunk
{
  uintmax_t x, y;
  tile tiles[CHUNK_WIDTH][CHUNK_HEIGHT];
};

terrain terrain_from_terrain_backstore (terrain_backstore);
size_t terrain_get_chunked (terrain, struct point, struct point,
                            ref (*)[*]);

void terrain_backstore_get (terrain_backstore, uintmax_t,
                            uintmax_t, size_t, size_t,
                            tile [static 1][*][*]);

#endif
