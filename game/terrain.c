/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#include <stdint.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "../common.h"
#include "../refcount.h"
#include "terrain.h"

struct terrain
{
  ref cache[10];
  terrain_backstore backstore;
};

terrain
terrain_from_terrain_backstore (register terrain_backstore backstore)
{
  register struct terrain *const tr = malloc (sizeof *tr);
  tr->backstore = backstore;
  memset (tr->cache, 0, sizeof tr->cache);
  return tr;
}

static ref
terrain_lookup_cached_chunk (register struct terrain const *const tr,
                             register uintmax_t const x,
                             register uintmax_t const y)
{
  for (register size_t i = 0; i < length (tr->cache); i++)
    if (tr->cache[i])
      {
        register struct chunk const *const chunk = ref_count_unwrap (tr->cache[i]);
        if (chunk->x == x * CHUNK_WIDTH && chunk->y == y * CHUNK_HEIGHT)
          return ref_count_share (tr->cache[i]);
      }
  return 0;
}

static ref
terrain_evict_cached_chunk (register struct terrain *const tr)
{
  register size_t const index = rand () % length (tr->cache);
  register struct chunk *const chunk = malloc (sizeof *chunk);
  if (tr->cache[index])
    ref_count_unshare (tr->cache[index]);
  return ref_count_share (tr->cache[index] = ref_count_wrap (chunk));
}

static ref
terrain_lookup_chunk (register struct terrain *const tr,
                      register uintmax_t const x,
                      register uintmax_t const y)
{
  register ref const r = terrain_lookup_cached_chunk (tr, x, y);
  if (!r)
    {
      register ref const r = terrain_evict_cached_chunk (tr);
      register struct chunk *const chunk = ref_count_unwrap (r);
      chunk->x = x * CHUNK_WIDTH;
      chunk->y = y * CHUNK_HEIGHT;
      terrain_backstore_get (tr->backstore,
                             x * CHUNK_WIDTH,
                             y * CHUNK_HEIGHT,
                             CHUNK_WIDTH,
                             CHUNK_HEIGHT,
                             &chunk->tiles);
      return r;
    }
  return r;
}

size_t
terrain_get_chunked (register terrain const tr,
                     register struct point const start,
                     register struct point const end,
                     register ref (*const dest)[(1 + end.x / CHUNK_WIDTH - start.x / CHUNK_WIDTH)
                                                                * (1 + end.y / CHUNK_HEIGHT - start.y / CHUNK_HEIGHT)])
{
  if (!dest)
    /* In ISO C, sizeof is said to evaluate its operand if it references
       a variable-length array. Accordingly, dereferencing a null pointer
       to a VLA produces undefined behavior. So we must compute the length
       of would-be *dest manually. */
    return (1 + end.x / CHUNK_WIDTH - start.x / CHUNK_WIDTH)
      * (1 + end.y / CHUNK_HEIGHT - start.y / CHUNK_HEIGHT);
  register size_t i = 0;
  for (register uintmax_t y = start.y / CHUNK_HEIGHT; y <= end.y / CHUNK_HEIGHT; y++)
    for (register uintmax_t x = start.x / CHUNK_WIDTH; x <= end.x / CHUNK_WIDTH; x++)
      assert ((*dest)[i++] = terrain_lookup_chunk ((struct terrain *)tr, x, y));
  assert (i == length (*dest));
  return i;
}
