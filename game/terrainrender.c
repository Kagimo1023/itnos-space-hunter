/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include "../video/video.h"
#include "terrain.h"
#include "terrainrender.h"

void
terrain_render_render (register render_context const context,
                       register terrain const tr,
                       register uintmax_t const ex,
                       register uintmax_t const ey,
                       register uintmax_t const ew,
                       register uintmax_t const eh,
                       register uintmax_t const scale)
{
  register uintmax_t const x = ex / scale, y = ey / scale;
  register uintmax_t const w = ew / scale + !!(ew % scale) + 1;
  register uintmax_t const h = eh / scale + !!(eh % scale) + 1;
  ref chunks[terrain_get_chunked (tr,
                                  (struct point const){ x, y },
                                  (struct point const){ x + w - 1, y + h - 1 },
                                  0)];
  terrain_get_chunked (tr,
                       (struct point const){ x, y },
                       (struct point const){ x + w - 1, y + h - 1 },
                       &chunks);
  for (register size_t i = 0; i < length (chunks); i++)
    {
      register struct chunk const *const chunk = ref_count_unwrap (chunks[i]);
      assert (chunk->x < x + w
              && chunk->y < y + h
              && chunk->x + CHUNK_WIDTH > x
              && chunk->y + CHUNK_HEIGHT > y);
      for (register uintmax_t ly = y > chunk->y ? y : chunk->y;
           ly < y + h && ly < chunk->y + CHUNK_HEIGHT; ly++)
        for (register uintmax_t lx = x > chunk->x ? x : chunk->x;
             lx < x + w && lx < chunk->x + CHUNK_WIDTH; lx++)
          {
            render_context_set_color (context,
                                      (struct rgba8){
                                        chunk->tiles[lx - chunk->x][ly - chunk->y],
                                        chunk->tiles[lx - chunk->x][ly - chunk->y],
                                        chunk->tiles[lx - chunk->x][ly - chunk->y],
                                        -1
                                      });
            register struct rectangle r = {
              { (lx - x) * scale, (ly - y) * scale },
              { (lx - x + 1) * scale, (ly - y + 1) * scale }
            };
            if (ex % scale)
              {
                r.p1.x = r.p1.x < ex % scale ? 0 : r.p1.x - ex % scale;
                r.p2.x -= ex % scale;
              }
            if (ey % scale)
              {
                r.p1.y = r.p1.y < ey % scale ? 0 : r.p1.y - ey % scale;
                r.p2.y -= ey % scale;
              }
            if (r.p2.x > ew)
              r.p2.x = ew;
            if (r.p2.y > eh)
              r.p2.y = eh;
            render_context_fill_rectangles (context, &(struct rectangle const[]){ r }, 1);
          }
      ref_count_unshare (chunks[i]);
    }
}
