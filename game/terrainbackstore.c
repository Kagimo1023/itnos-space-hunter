/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#include <stdlib.h>
#include <stdint.h>
#include <limits.h>
#include "terrain.h"

void
terrain_backstore_get (register struct terrain_backstore const *const bs,
                       register uintmax_t const x, register uintmax_t const y,
                       register size_t const width, register size_t const height,
                       register tile (*const dest)[width][height])
{
  if (dest)
    for (register size_t y = 0; y < height; y++)
      for (register size_t x = 0; x < width; x++)
        (*dest)[x][y] = rand () % UCHAR_MAX;
}
