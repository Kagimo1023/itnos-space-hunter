CC=gcc
CFLAGS=-O2 -flto -fstack-protector-strong -fstack-clash-protection -D_FORTIFY_SOURCE=2 -std=c17 -Wpedantic -Wreturn-type -Wnull-dereference -Wstrict-aliasing -Wwrite-strings -Wcast-function-type -Wignored-qualifiers -Wstrict-prototypes -Wstrict-prototypes -Wold-style-declaration -Wshift-overflow=2 -Wcast-align=strict -Warray-bounds=2 -Wformat=2 -Wformat-overflow=2 -Wformat-truncation=2 -Wformat-signedness -Wno-format-zero-length -Wno-format-extra-args -Wno-format-contains-nul $$(pkg-config --cflags sdl2)
timestamp: $(OBJECTS) $(SUBDIRS)
	touch timestamp
depend: FORCE
	gcc -MM $(OBJECTS:.o=.c) >Makefile.dep
	@for d in $(SUBDIRS); do cd "$$d"; make depend; cd -; done
$(SUBDIRS): FORCE
	cd $@ && $(MAKE)
FORCE: ;
include Makefile.dep
