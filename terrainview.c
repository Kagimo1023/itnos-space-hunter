/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <threads.h>
#include "video/video.h"
#include "input/input.h"
#include "game/terrain.h"
#include "game/terrainrender.h"
#include "event/event.h"
#include "event/eventsource.h"
#include "event/displayevent.h"
#include "event/pointerevent.h"
#include "event/timerevent.h"
#include "application/application.h"
#include "toplevel.h"

display sdl_init_default_display (void);

struct terrain_view_state
{
  display d;
  display_surface s;
  render_context rc;
  uintmax_t scale;
  terrain tr;
  uintmax_t x, y;
  _Bool repaint;
};

static void
start (register struct terrain_view_state *const state)
{
  state->s = display_create_display_surface (state->d, "Hello world", 400, 400);
  state->rc = display_surface_get_render_context (state->s);
  render_context_set_color (state->rc, (struct rgba8){ 0, 0, 0, -1 });
  render_context_clear (state->rc);
  render_context_present (state->rc);
  fprintf (stderr, "Started\n");
}


static void
repaint (register struct terrain_view_state *const state)
{
  terrain_render_render (state->rc, state->tr, state->x, state->y,
                         display_surface_get_width (state->s),
                         display_surface_get_height (state->s),
                         state->scale);
  render_context_present (state->rc);
}

static void
init (register void *const arg)
{
  register struct terrain_view_state *const state = arg;
  srand (time (0));
  state->d = 0;
  state->tr = terrain_from_terrain_backstore (0);
  state->x = 0;
  state->y = 0;
  state->scale = 30;
  fprintf (stderr, "Initialized\n");
}

static void
shutdown (register void *const ignored)
{
  fprintf (stderr, "Shutting down\n");
}

static void
update (register void *const arg,
        register event const e)
{
  struct terrain_view_state *const state = arg;
  fprintf (stderr, "Received an event\n");
  register event_source const source = event_get_source (e);
  fprintf (stderr, "Source: %p (%s)\n",
           (void *)source,
           event_source_get_name (source));
  switch (event_source_get_type (source))
    {
    case EVENT_SOURCE_DISPLAY:
      fprintf (stderr, "Event type is display\n");
      switch (display_event_get_type (e))
        {
        case DISPLAY_EVENT_CONNECTED:
          if (!state->d)
            {
              state->d = display_event_source_get_display (source);
              fprintf (stderr, "Connected to display %p\n", (void *)state->d);
              start (state);
              repaint (state);
            }
          break;
        case DISPLAY_EVENT_DISPLAY_SURFACE_RESIZED:
          if (display_surface_id_equals (state->s,
                                         display_event_display_surface_resized_get_display_surface_id (e)))
            state->repaint = 1;
        }
      break;
    case EVENT_SOURCE_POINTER:
      fprintf (stderr, "Event type is pointer\n");
      switch (pointer_event_get_type (e))
        {
        case POINTER_EVENT_MOVEMENT:;
          if (pointer_get_if_locked (pointer_event_source_get_pointer (source)))
            {
              register intmax_t const dx = pointer_event_movement_get_delta_x (e);
              register intmax_t const dy = pointer_event_movement_get_delta_y (e);
              if (dx < 0)
                if (state->x < (uintmax_t)-dx)
                  state->x = 0;
                else
                  state->x -= (uintmax_t)-dx;
              else
                state->x += dx;
              if (dy < 0)
                if (state->y < (uintmax_t)-dy)
                  state->y = 0;
                else
                  state->y -= (uintmax_t)-dy;
              else
                state->y += (uintmax_t)dy;
              state->repaint = 1;
            }
          break;
        case POINTER_EVENT_WHEEL_MOVEMENT:;
          register intmax_t const d = pointer_event_wheel_movement_get_delta (e);
          if (d < 0)
            if (state->scale <= (uintmax_t)-d)
              state->scale = 1;
            else
              state->scale -= (uintmax_t)-d;
          else
            state->scale += (uintmax_t)d;
          state->repaint = 1;
          break;
        case POINTER_EVENT_CLICK:
          pointer_try_lock (pointer_event_source_get_pointer (source));
          break;
        case POINTER_EVENT_RELEASE:
          pointer_try_unlock (pointer_event_source_get_pointer (source));
        }
      break;
    case EVENT_SOURCE_TIMER:
      fprintf (stderr, "Event type is timer\n");
      fprintf (stderr, "%ju ms elapsed\n", timer_event_periodic_get_elapsed_time (e));
      if (state->repaint)
        {
          fprintf (stderr, "Repainting\n");
          repaint (state);
          state->repaint = 0;
        }
    }
}

int
main (void)
{
  struct terrain_view_state state = { 0 };
  application terrain_view = application_create (init,
                                                 shutdown,
                                                 update,
                                                 &state);
  return toplevel_run (terrain_view) ? 0 : EXIT_FAILURE;
}
