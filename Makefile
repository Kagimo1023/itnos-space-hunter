.POSIX:
TERRAINVIEW_OBJECTS=terrainview.o toplevel.o event/eventprovider.o game/terrain.o\
game/terrainbackstore.o game/terrainrender.o video/video.o video/rendercontext.o\
application/application.o event/displayevent.o event/eventsource.o event/event.o\
event/timerevent.o event/pointerevent.o platform/genericevents.o input/input.o\
platform/sdl/sdlevents.o platform/sdl/sdldisplay.o platform/sdl/sdlrenderer.o\
platform/sdl/sdlinput.o refcount.o
LIBS=$$(pkg-config --libs sdl2)
terrainview: timestamp
	$(CC) $(LDFLAGS) -o $@ $(TERRAINVIEW_OBJECTS) $(LIBS)
OBJECTS=terrainview.o toplevel.o refcount.o
SUBDIRS=application event game platform input video
include Makefile.inc
