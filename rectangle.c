/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>
   Copyright (C) 2021 Daniil Alexeev <daniilxcxc2@gmail.com>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#include <stdint.h>
#include <stdlib.h>
#include <threads.h>
#include "video.h"

display sdl_init_default_display (void);

int
main (void)
{  
  register display const d = sdl_init_default_display ();
  register display_surface const surface = display_create_display_surface (d, "Hello world", 800, 600);
  register render_context const context = display_surface_get_render_context (surface);
  render_context_set_color (context, (struct rgba8){ 0, 0, 0, -1 });
  render_context_clear (context);

  struct rectangle const rect[] = {
    { 50, 50 },
    { 50, 50 }
  };
  
  render_context_set_color (context, (struct rgba8){ -1, 0, 0, -1 });
  render_context_fill_rectangles (context, &rect, sizeof rect / sizeof *rect);
  
  render_context_present (context);
  thrd_sleep (&(struct timespec){ .tv_sec = 10 }, 0);
}
