/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#include <stdlib.h>
#include <threads.h>
#include "common.h"
#include "event/event.h"
#include "event/eventprovider.h"
#include "application/application.h"
#include "toplevel.h"

extern struct event_provider const sdl_event_provider;
extern struct event_provider const generic_event_provider;

static event_provider const event_providers[] = {
  &generic_event_provider,
  &sdl_event_provider
};

static void
handler (register event const e, register void *const a)
{
  register application const *const app = a;
  application_dispatch_event (*app, e);
}

_Bool
toplevel_run (application app)
{
  application_init (app);
  for (register size_t i = 0; i < length (event_providers); i++)
    {
      event_provider_set_callback (event_providers[i], handler, &app);
      event_provider_init (event_providers[i]);
    }
  for (;;)
    {
      for (register size_t i = 0; i < length (event_providers); i++)
        event_provider_update (event_providers[i]);
      thrd_sleep (&(struct timespec const){ .tv_nsec = 5000000 }, 0);
    }
  return 1;
}
