/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#define SH2_APPLICATION_PRIVATE
#include <stdlib.h>
#include "application.h"

struct application const *
application_create (register void (*const init)(void *),
                    register void (*const shutdown)(void *),
                    register void (*const dispatch_event)(void *, event),
                    register void *const initial_state)
{
  register struct application *app = malloc (sizeof *app);
  app->init = init;
  app->shutdown = shutdown;
  app->dispatch_event = dispatch_event;
  app->state = initial_state;
  return app;
}

void
application_init (register struct application const *const app)
{
  app->init (app->state);
}

void
application_shutdown (register struct application const *const app)
{
  app->shutdown (app->state);
}

void
application_dispatch_event (register struct application const *const app,
                            register event const e)
{
  app->dispatch_event (app->state, e);
}
