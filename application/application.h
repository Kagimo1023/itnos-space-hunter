/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#ifndef SH2_APPLICATION_H
#define SH2_APPLICATION_H

#include "../event/event.h"

typedef struct application const *application;

application application_create (void (*)(void *),
                                void (*)(void *),
                                void (*)(void *, event),
                                void *);
void application_init (application);
void application_shutdown (application);
void application_dispatch_event (application, event);

#ifdef SH2_APPLICATION_PRIVATE

struct application
{
  void (*init)(void *);
  void (*shutdown)(void *);
  void (*dispatch_event)(void *, event);
  void *state;
};

#endif

#endif
