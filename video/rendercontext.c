/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>
   Copyright (C) 2021 Daniil Alexeev <daniilxcxc2@gmail.com>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#define SH2_VIDEO_PRIVATE
#include <stdint.h>
#include <stdlib.h>
#include "video.h"

struct texture const *
render_context_create_texture (register render_context const context,
                               register char * pixels,
                               register const size_t width,
                               register const size_t height)
{
  register struct texture *const t = malloc (sizeof *t);
  t->context = context;
  t->handle = context->renderer->create_texture (context->handle,
                                                 pixels,
                                                 width,
                                                 height);
  return t;
}

struct texture const *
render_context_create_monochrome_texture (register render_context const context,
                                          register char * pixels,
                                          register const size_t width,
                                          register const size_t height)
{
  register struct texture *const t = malloc (sizeof *t);
  t->context = context;
  t->handle = context->renderer->create_monochrome_texture (context->handle,
                                                            pixels,
                                                            width,
                                                            height);
  return t;
}

void
texture_draw (register texture const t,
              register uintmax_t const x,
              register uintmax_t const y)
{
  t->context->renderer->draw_texture (t->context->handle,
                                      t->handle, x, y);
}

void
render_context_fill_rectangles (context, rects, size)
    register render_context const context;
    register size_t const size;
    register struct rectangle const (*const rects)[size];
{
  context->renderer->fill_rectangles (context->handle, rects, size);
}

void
render_context_set_color (register render_context const context,
                          register struct rgba8 const color)
{
  context->renderer->set_color (context->handle, color);
}

void
render_context_clear (register render_context const context)
{
  context->renderer->clear (context->handle);
}

void
render_context_draw_path (context, points, length)
     register render_context const context;
     register size_t const length;
     register struct point const (*points)[length];
{
  context->renderer->draw_path (context->handle,
                                points,
                                length);
}

void
render_context_present (register render_context const context)
{
  context->renderer->present (context->handle);
}
