/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#define SH2_VIDEO_PRIVATE
#include <stdlib.h>
#include <stdint.h>
#include "video.h"

uintmax_t
display_get_width (register struct display const *const d)
{
  return d->driver->display_get_width (d->handle);
}

uintmax_t
display_get_height (register struct display const *const d)
{
  return d->driver->display_get_height (d->handle);
}

struct display_surface const *
display_create_display_surface (register struct display const *const d,
                                register char const *const name,
                                register uintmax_t const width,
                                register uintmax_t const height)
{
  register struct display_surface *const s = malloc (sizeof *s);
  s->driver = d->driver;
  s->handle = d->driver->display_create_display_surface (d->handle,
                                                         name,
                                                         width,
                                                         height);
  return s;
}

_Bool
display_surface_id_equals (register struct display_surface const *const s,
                           register display_surface_id const id)
{
  return s->driver->display_surface_id_equals (s->handle, id);
}

display_surface_id
display_surface_get_id (register struct display_surface const *const s)
{
  return s->driver->display_surface_get_id (s->handle);
}

uintmax_t
display_surface_get_width (register struct display_surface const *const s)
{
  return s->driver->display_surface_get_width (s->handle);
}

uintmax_t
display_surface_get_height (register struct display_surface const *const s)
{
  return s->driver->display_surface_get_height (s->handle);
}

render_context
display_surface_get_render_context (register struct display_surface const *const s)
{
  return s->driver->display_surface_get_render_context (s->handle);
}

void
display_surface_resize (register struct display_surface const *const s,
                        uintmax_t width,
                        uintmax_t height)
{
  s->driver->display_surface_resize (s->handle, width, height);
}

void
display_surface_destroy (register struct display_surface const *const s)
{
  s->driver->display_surface_destroy (s->handle);
}
