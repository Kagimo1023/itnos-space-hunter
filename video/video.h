/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>
   Copyright (C) 2021 Daniil Alexeev <daniilxcxc2@gmail.com>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#ifndef SH2_VIDEO_H
#define SH2_VIDEO_H

#include <stddef.h>
#include "../common.h"

typedef struct display const *display;
typedef struct display_surface const *display_surface;
typedef union
{
  void *p;
  uintmax_t u;
} display_surface_id;
typedef struct render_context const *render_context;
typedef struct texture const *texture;

struct rgba8
{
  unsigned char r, g, b, a;
};

uintmax_t display_get_width (display);
uintmax_t display_get_height (display);
display_surface display_create_display_surface (display, char const *name,
                                                uintmax_t, uintmax_t);
void display_destroy (display);

_Bool display_surface_id_equals (display_surface, display_surface_id);
display_surface_id display_surface_get_id (display_surface);
uintmax_t display_surface_get_width (display_surface);
uintmax_t display_surface_get_height (display_surface);
render_context display_surface_get_render_context (display_surface);
void display_surface_resize (display_surface, uintmax_t, uintmax_t);
void display_surface_destroy (display_surface);

texture render_context_create_texture (render_context,
                                       char *pixels,
                                       size_t width,
                                       size_t height);
texture render_context_create_monochrome_texture (render_context,
                                                  char *pixels,
                                                  size_t width,
                                                  size_t height);
void texture_draw (texture, uintmax_t, uintmax_t);
void render_context_set_color (render_context, struct rgba8);
void render_context_fill_rectangles (render_context, struct rectangle const (*)[*], size_t);
void render_context_clear (render_context);
void render_context_draw_path (render_context, struct point const (*)[*],
                               size_t);
void render_context_present (render_context);

#ifdef SH2_VIDEO_PRIVATE

union handle
{
  void *p;
  uintmax_t u;
};

struct driver
{
  uintmax_t (*display_get_width)(union handle);
  uintmax_t (*display_get_height)(union handle);
  union handle (*display_create_display_surface)(union handle, char const *,
                                                 uintmax_t, uintmax_t);
  _Bool (*display_surface_id_equals)(union handle, display_surface_id);
  display_surface_id (*display_surface_get_id)(union handle);
  uintmax_t (*display_surface_get_width)(union handle);
  uintmax_t (*display_surface_get_height)(union handle);
  render_context (*display_surface_get_render_context)(union handle);
  void (*display_surface_resize)(union handle, uintmax_t, uintmax_t);
  void (*display_surface_destroy)(union handle);
};

struct renderer
{
  union handle (*create_texture)(union handle, char *, const size_t, const size_t);
  union handle (*create_monochrome_texture)(union handle, char *, const size_t, const size_t);
  void (*draw_texture)(union handle, union handle, uintmax_t, uintmax_t);
  void (*fill_rectangles)(union handle, struct rectangle const (*)[*], size_t);
  void (*set_color)(union handle, struct rgba8);
  void (*clear)(union handle);
  void (*draw_path)(union handle, struct point const (*)[*], size_t);
  void (*present)(union handle);
};

struct display
{
  struct driver const *driver;
  union handle handle;
};

struct display_surface
{
  struct driver const *driver;
  union handle handle;
};

struct render_context
{
  struct renderer const *renderer;
  union handle handle;
};

struct texture
{
  struct render_context const *context;
  union handle handle;
};

#endif

#endif
