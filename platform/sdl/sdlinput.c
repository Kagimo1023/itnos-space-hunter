/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#define SH2_INPUT_PRIVATE
#include <SDL.h>
#include "../../input/input.h"
#include "sdlplatform.h"

static _Bool sdl_pointer_try_lock (union handle);
static _Bool sdl_pointer_try_unlock (union handle);
static _Bool sdl_pointer_get_if_locked (union handle);

static struct input_driver sdl_input_driver = {
  .pointer_try_lock = sdl_pointer_try_lock,
  .pointer_try_unlock = sdl_pointer_try_unlock,
  .pointer_get_if_locked = sdl_pointer_get_if_locked
};

struct pointer const *
sdl_init_mouse (void)
{
  register struct pointer *p = malloc (sizeof *p);
  p->driver = &sdl_input_driver;
  return p;
}

static _Bool
sdl_pointer_try_lock (register union handle const handle)
{
  return !SDL_SetRelativeMouseMode (SDL_TRUE);
}

static _Bool
sdl_pointer_try_unlock (register union handle const handle)
{
  return !SDL_SetRelativeMouseMode (SDL_FALSE);
}

static _Bool
sdl_pointer_get_if_locked (register union handle const handle)
{
  return SDL_GetRelativeMouseMode ();
}
