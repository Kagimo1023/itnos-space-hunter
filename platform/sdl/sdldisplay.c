/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#define SH2_VIDEO_PRIVATE
#include <stdint.h>
#include <limits.h>
#include <assert.h>
#include <SDL.h>
#include "../../video/video.h"
#include "sdlplatform.h"

extern struct renderer const *const sdl_renderer;

static uintmax_t sdl_display_get_width (union handle);
static uintmax_t sdl_display_get_height (union handle);
static union handle sdl_display_create_display_surface (union handle,
                                                        char const *,
                                                        uintmax_t,
                                                        uintmax_t);
static _Bool sdl_display_surface_id_equals (union handle, display_surface_id);
static display_surface_id sdl_display_surface_get_id (union handle);
static uintmax_t sdl_display_surface_get_width (union handle);
static uintmax_t sdl_display_surface_get_height (union handle);
static render_context sdl_display_surface_get_render_context (union handle);
static void sdl_display_surface_resize (union handle, uintmax_t, uintmax_t);
static void sdl_display_surface_destroy (union handle);

static struct driver const sdl_display =
  {
    .display_get_width = sdl_display_get_width,
    .display_get_height = sdl_display_get_height,
    .display_create_display_surface = sdl_display_create_display_surface,
    .display_surface_id_equals = sdl_display_surface_id_equals,
    .display_surface_get_id = sdl_display_surface_get_id,
    .display_surface_get_width = sdl_display_surface_get_width,
    .display_surface_get_height = sdl_display_surface_get_height,
    .display_surface_get_render_context = sdl_display_surface_get_render_context,
    .display_surface_resize = sdl_display_surface_resize,
    .display_surface_destroy = sdl_display_surface_destroy
  };

display
sdl_init_default_display (void)
{
  SDL_Init (SDL_INIT_VIDEO);
  register struct display *d = malloc (sizeof *d);
  d->driver = &sdl_display;
  d->handle.u = 0;
  return d;
}

static uintmax_t
sdl_display_get_width (register union handle const handle)
{
  assert (handle.u < INT_MAX);
  SDL_Rect bounds;
  SDL_GetDisplayUsableBounds (handle.u, &bounds);
  return bounds.w;
}

static uintmax_t
sdl_display_get_height (register union handle const handle)
{
  assert (handle.u < INT_MAX);
  SDL_Rect bounds;
  SDL_GetDisplayUsableBounds (handle.u, &bounds);
  return bounds.h;
}

static union handle
sdl_display_create_display_surface (register union handle const handle,
                                    register char const *const title,
                                    register uintmax_t const width,
                                    register uintmax_t const height)
{
  assert (handle.u < INT_MAX);
  register SDL_Window *const window =  SDL_CreateWindow (title,
                                                         SDL_WINDOWPOS_UNDEFINED_DISPLAY (handle.u),
                                                         SDL_WINDOWPOS_UNDEFINED_DISPLAY (handle.u),
                                                         width > INT_MAX ? INT_MAX : width,
                                                         height > INT_MAX ? INT_MAX : height,
                                                         SDL_WINDOW_RESIZABLE);
  SDL_CreateRenderer (window, -1, 0);
  return (union handle){ .p = window };
}

static struct render_context const *
sdl_display_surface_get_render_context (register union handle const handle)
{
  struct render_context *const context = malloc (sizeof *context);
  context->renderer = sdl_renderer;
  context->handle.p = SDL_GetRenderer (handle.p);
  return context;
}

static _Bool
sdl_display_surface_id_equals (register union handle const handle,
                               register display_surface_id const id)
{
  return SDL_GetWindowID (handle.p) == id.u;
}

static display_surface_id
sdl_display_surface_get_id (register union handle const handle)
{
  return (display_surface_id const){ .u = SDL_GetWindowID (handle.p) };
}

static uintmax_t
sdl_display_surface_get_width (register union handle const handle)
{
  int width;
  SDL_GetWindowSize (handle.p, &width, 0);
  return width;
}

static uintmax_t
sdl_display_surface_get_height (register union handle const handle)
{
  int height;
  SDL_GetWindowSize (handle.p, 0, &height);
  return height;
}

static void sdl_display_surface_resize (register union handle const handle,
                                        register uintmax_t const width,
                                        register uintmax_t const height)
{
  SDL_SetWindowSize (handle.p,
                     width > INT_MAX ? INT_MAX : width,
                     height > INT_MAX ? INT_MAX : height);
}

static void
sdl_display_surface_destroy (register union handle const handle)
{
  SDL_DestroyWindow (handle.p);
}
