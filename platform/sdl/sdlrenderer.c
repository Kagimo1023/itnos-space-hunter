/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>
   Copyright (C) 2021 Daniil Alexeev <daniilxcxc2@gmail.com>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#define SH2_VIDEO_PRIVATE
#include <stdint.h>
#include <stdlib.h>
#include <limits.h>
#include <assert.h>
#include <SDL.h>
#include "../../video/video.h"

static union handle sdl_renderer_create_texture (union handle, char *, size_t, size_t);
static union handle sdl_renderer_create_monochrome_texture (union handle, char *, size_t, size_t);
static void sdl_renderer_draw_texture (union handle, union handle, uintmax_t, uintmax_t);
static void sdl_renderer_set_color (union handle, struct rgba8);
static void sdl_renderer_clear (union handle);
static void sdl_renderer_draw_path (union handle, struct point const (*)[*], size_t);
static void sdl_renderer_fill_rectangles (union handle, struct rectangle const (*)[*], size_t);
static void sdl_renderer_present (union handle);

static struct renderer const sdl_renderer_impl =
  {
    .create_texture = sdl_renderer_create_texture,
    .create_monochrome_texture = sdl_renderer_create_monochrome_texture,
    .draw_texture = sdl_renderer_draw_texture,
    .set_color = sdl_renderer_set_color,
    .clear = sdl_renderer_clear,
    .draw_path = sdl_renderer_draw_path,
    .fill_rectangles = sdl_renderer_fill_rectangles,
    .present = sdl_renderer_present
  };
struct renderer const *const sdl_renderer = &sdl_renderer_impl;

static union handle
sdl_renderer_create_texture (register union handle const handle,
                             register char *const pixels,
                             register size_t const width,
                             register size_t const height)
{
  if (width > INT_MAX / 4 || height > INT_MAX)
    return (union handle const){ 0 };
  register SDL_Surface *const surface = SDL_CreateRGBSurfaceFrom(pixels,
                                                                 width,
                                                                 height,
                                                                 32,
                                                                 width * 4,
                                                                 0xff,
                                                                 0xff00,
                                                                 0xff0000,
                                                                 0xff000000);
  register SDL_Texture *const texture = SDL_CreateTextureFromSurface(handle.p, surface);
  SDL_FreeSurface(surface);
  return (union handle const){ .p = texture };
}

static union handle
sdl_renderer_create_monochrome_texture (register union handle const handle,
                                        register char *const pixels,
                                        register size_t const width,
                                        register size_t const height)
{
  if (width > INT_MAX / 4 || height > INT_MAX)
    return (union handle const){ 0 };
  register SDL_Surface *const surface = SDL_CreateRGBSurfaceFrom(pixels, width, height, 1,
                                                                 width / CHAR_BIT + !!(width % CHAR_BIT),
                                                                 1, 1, 1, 0);
  register SDL_Texture *const texture = SDL_CreateTextureFromSurface(handle.p, surface);
  SDL_FreeSurface(surface);
  return (union handle const){ .p = texture };
}

static void
sdl_renderer_draw_texture (register union handle const handle,
                           register union handle const t,
                           register uintmax_t const x,
                           register uintmax_t const y)
{
  assert (handle.p);
  assert (t.p);
  if (x >= INT_MAX || y >= INT_MAX)
    return;
  int width = 0, height = 0;
  SDL_QueryTexture (t.p, 0, 0, &width, &height);
  assert (width > 0 && height > 0);
  SDL_RenderCopy (handle.p, t.p, 0,
                  &(struct SDL_Rect const){
                    .x = x, .y = y,
                    .w = width,
                    .h = height
                  });
}

static void
sdl_renderer_set_color (register union handle const handle,
                        register struct rgba8 const color)
{
  assert (handle.p);
  SDL_SetRenderDrawColor (handle.p,
                          color.r,
                          color.g,
                          color.b,
                          color.a);
}

static void
sdl_renderer_clear (register union handle const handle)
{
  SDL_RenderClear (handle.p);
}

static void
sdl_renderer_fill_rectangles (handle, rects, size)
     register union handle const handle;
     register size_t size;
     register struct rectangle const (*const rects)[size];
{
  register uintmax_t const origsize = size;
  if (size > INT_MAX)
      size = INT_MAX;

  SDL_Rect sdl_rects[size];
  
  for (register size_t i = 0; i < size; i++)
    {
      if (((*rects)[i].p1.x >= INT_MAX
           && (*rects)[i].p2.x >= INT_MAX)
          || ((*rects)[i].p1.y >= INT_MAX
              && (*rects)[i].p2.y >= INT_MAX))
        {
          sdl_rects[i] = (SDL_Rect const){ 0 };
          continue;
        }

      register uintmax_t const width = rectangle_width ((*rects)[i]);
      register uintmax_t const height = rectangle_height ((*rects)[i]);

      if((*rects)[i].p1.x > (*rects)[i].p2.x)
        sdl_rects[i].x = (*rects)[i].p2.x;
      else
        sdl_rects[i].x = (*rects)[i].p1.x;
      sdl_rects[i].w = width > INT_MAX ? INT_MAX : width;

      if((*rects)[i].p1.y > (*rects)[i].p2.y)
        sdl_rects[i].y = (*rects)[i].p2.y;
      else
        sdl_rects[i].y = (*rects)[i].p1.y;
      sdl_rects[i].h = height > INT_MAX ? INT_MAX : height;
    }
  SDL_RenderFillRects(handle.p, sdl_rects, size);
  if (size < origsize)
    sdl_renderer_fill_rectangles (handle,
                                  (struct rectangle const (*)[origsize - size])(*rects + size),
                                  origsize - size);
}

static void
sdl_renderer_draw_path (handle, points, length)
     register union handle const handle;
     register size_t length;
     register struct point const (*const points)[length];
{
  register size_t const origlength = length;
  if (length > INT_MAX)
    {
      length = INT_MAX;
    }
  SDL_Point sdl_points[length];
  for (register size_t i = 0; i < length; i++)
    {
      sdl_points[i].x = (*points)[i].x > INT_MAX ? INT_MAX : (*points)[i].x;
      sdl_points[i].y = (*points)[i].y > INT_MAX ? INT_MAX : (*points)[i].y;
    }
  SDL_RenderDrawLines (handle.p, sdl_points, length);
  if (origlength > length)
    sdl_renderer_draw_path (handle,
                            (struct point const (*)[origlength - length])(*points + length),
                            origlength - length);
}

static void
sdl_renderer_present (register union handle const handle)
{
  SDL_RenderPresent (handle.p);
}
