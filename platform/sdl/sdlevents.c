/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#define SH2_EVENT_PROVIDER_PRIVATE
#include <SDL.h>
#include "../../video/video.h"
#include "../../input/input.h"
#include "../../event/eventprovider.h"
#include "../../event/eventsource.h"
#include "../../event/displayevent.h"
#include "../../event/pointerevent.h"
#include "sdlplatform.h"

static event_source display_event_source = 0;
static event_source pointer_event_source = 0;
static event_provider_callback callback = 0;
static void *callback_argument = 0;
static void init (void);
static void set_callback (event_provider_callback, void *);
static void update (void);

struct event_provider sdl_event_provider = {
  .init = init,
  .set_callback = set_callback,
  .update = update
};

static int
event_filter (register void *const ignored, register SDL_Event *const sdl_event)
{
  register event e = 0;
  switch (sdl_event->type)
    {
    case SDL_QUIT:
      exit (0);
    case SDL_WINDOWEVENT:
      switch (sdl_event->window.event)
        {
        case SDL_WINDOWEVENT_RESIZED:
          e = display_event_display_surface_resized_create (display_event_source,
                                                            (display_surface_id){ .u = sdl_event->window.windowID },
                                                            sdl_event->window.data1,
                                                            sdl_event->window.data2);
        }
      break;
    case SDL_MOUSEMOTION:
      e = pointer_event_movement_create (pointer_event_source,
                                         sdl_event->motion.xrel,
                                         sdl_event->motion.yrel);
      break;
    case SDL_MOUSEWHEEL:
      e = pointer_event_wheel_movement_create (pointer_event_source,
                                               sdl_event->wheel.direction == SDL_MOUSEWHEEL_NORMAL ?
                                               sdl_event->wheel.y : -sdl_event->wheel.y);
      break;
    case SDL_MOUSEBUTTONDOWN:
      e = pointer_event_click_create (pointer_event_source);
      break;
    case SDL_MOUSEBUTTONUP:
      e = pointer_event_release_create (pointer_event_source);
    }
  if (e)
    {
      if (callback)
        callback (e, callback_argument);
      event_free (e);
    }
  return 1;
}

static void
init (void)
{
  SDL_Init (SDL_INIT_EVENTS);
  display_event_source = display_event_source_create ("SDL display",
                                                      sdl_init_default_display ());
  if (callback)
    {
      register event const e = display_event_connected_create (display_event_source);
      callback (e,
                callback_argument);
      event_free (e);
    }
  pointer_event_source = pointer_event_source_create ("SDL mouse", sdl_init_mouse ());
  SDL_SetEventFilter (event_filter, 0);
}

static void
set_callback (register event_provider_callback const cb,
              register void *const user_argument)
{
  callback = cb;
  callback_argument = user_argument;
}

static void
update (void)
{
  SDL_PumpEvents ();
}
