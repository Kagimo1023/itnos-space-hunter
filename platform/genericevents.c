/* Copyright (C) 2021 Boris Stepanov <kanichos@yandex.ru>

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#define SH2_EVENT_PROVIDER_PRIVATE
#include <stdint.h>
#include <time.h>
#include "../event/event.h"
#include "../event/eventsource.h"
#include "../event/timerevent.h"
#include "../event/eventprovider.h"

static void init (void);
static void set_callback (event_provider_callback, void *);
static void update (void);

struct event_provider generic_event_provider = {
  .init = init,
  .set_callback = set_callback,
  .update = update
};

static event_provider_callback callback = 0;
static void *callback_argument;
static event_source timer;
static struct timespec last;

static void
init (void)
{
  timer = timer_event_source_create ("System timer");
  timespec_get (&last, TIME_UTC);
}

static void
set_callback (register event_provider_callback const cb,
              register void *const user_argument)
{
  callback = cb;
  callback_argument = user_argument;
}

static void
update (void)
{
  static uint_fast16_t const rate = 60;
  struct timespec ts;
  timespec_get (&ts, TIME_UTC);
  uint_fast64_t diff = ((ts.tv_sec - last.tv_sec) * 1000000000
                        + (ts.tv_nsec - last.tv_nsec));
  if (diff >= 1000000000 / rate)
    {
      last = ts;
      register event const e = timer_event_periodic_create (timer, diff / 1000000);
      if (callback)
        callback (e, callback_argument);
      event_free (e);
    }
}
